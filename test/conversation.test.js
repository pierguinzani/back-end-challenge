process.env.NODE_ENV = 'production';
var mongoose = require('mongoose');
const { Conversation } = require("../src/models/conversation");

var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../src/index');
var should = chai.should();

chai.use(chaiHttp);

describe('Conversations', function () {
  beforeEach(function (done) {
    Conversation.deleteMany({}, function (err, removed) {
      done();
    });
  });

  /** 
   * Teste da rota: /GET
   */
  describe('/GET conversations', function () {
    it('Must return all conversations', function (done) {
      chai.request(server)
        .get('/conversation')
        .end(function (error, res) {
          res.should.have.status(200);
          res.body.should.be.a('array');
          done();
        });
    });
  });

  /** 
  * Teste da rota: /GET/:id
  */
  describe('/GET/:id Conversation', function () {
    it('Must return a conversation by id', function (done) {
      var conversation = new Conversation({
        "name": "Cicrano",
        "cpf": "12345678910",
        "email": "example@example.com",
        "phone": 5598991234567,
        "state": "MARANHÃO",
        "subject": "reclamação"
      })
      conversation.save(function (error, conversation) {
        chai.request(server)
          .get('/conversation/' + conversation.id)
          .send(conversation)
          .end(function (error, res) {
            res.should.be.a('object');
            res.body.should.have.property('name');
            res.body.should.have.property('cpf');
            res.body.should.have.property('email');
            res.body.should.have.property('phone');
            res.body.should.have.property('state');
            res.body.should.have.property('subject');
            res.body.should.have.property('_id').eql(conversation.id);
            done();
          });
      });
    });
  });

  /** 
     * Teste da rota: /GET/pending
     */
  describe('/GET/pending Conversation', function () {
    it('Must return all conversations pending', function (done) {
      var conversation = new Conversation({
        "name": "Cicrano",
        "cpf": "12345678910",
        "email": "example@example.com",
        "phone": 5598991234567,
        "state": "MARANHÃO",
        "subject": "reclamação"
      })
      conversation.save(function (error, conversation) {
        chai.request(server)
          .get('/conversation/pending/')
          .send(conversation)
          .end(function (error, res) {
            res.should.be.a('object');
            res.should.have.status(200);
            res.body.should.be.a('array');
            done();
          });
      });
    });
  });

  /** 
     * Teste da rota: /GET/lastconversation/:cpf
     */
  describe('/GET/lastconversation/:cpf Conversation', function () {
    it('Must return the last conversation of the customer by cpf', function (done) {
      var conversation = new Conversation({
        "name": "Cicrano",
        "cpf": "12345678910",
        "email": "example@example.com",
        "phone": 5598991234567,
        "state": "MARANHÃO",
        "subject": "reclamação"
      })
      conversation.save(function (error, conversation) {
        chai.request(server)
          .get('/conversation/lastconversation/' + conversation.cpf)
          .send(conversation)
          .end(function (error, res) {
            res.should.be.a('object');
            res.body.should.have.property('name');
            res.body.should.have.property('cpf');
            res.body.should.have.property('email');
            res.body.should.have.property('phone');
            res.body.should.have.property('state');
            res.body.should.have.property('subject');
            done();
          });
      });
    });
  });

  /** 
  * Teste da rota: /GET/findconversation/:name
  */
  describe('/GET/findconversation/:name Conversation', function () {
    it('must return the search for a customer through his full or pacial name', function (done) {
      var conversation = new Conversation({
        "name": "Cicrano",
        "cpf": "12345678910",
        "email": "example@example.com",
        "phone": 5598991234567,
        "state": "MARANHÃO",
        "subject": "reclamação"
      })
      conversation.save(function (error, conversation) {
        chai.request(server)
          .get('/conversation/findconversation/' + 'Cicr')
          .send(conversation)
          .end(function (error, res) {
            res.should.be.a('object');
            res.should.be.a('object');
            res.should.have.status(200);
            res.body.should.be.a('array');
            done();
          });
      });
    });
  });

  /** 
   * Teste da rota: /POST
   */
  describe('/POST Conversation', function () {
    it('the application should not return the POST of the created conversation, since a field has not been defined and all fields are mandatory.', function (done) {

      let conversation = {
        "name": "Fulano",
        "cpf": "12345678910",
        "email": "example@example.com",
        "phone": 5598991234567,
        "state": "MARANHÃO",
        //subject: "elogio"
      }
      chai.request(server)
        .post('/conversation')
        .send(conversation)
        .end(function (error, res) {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have.property('errors');
          res.body.errors.should.have.property('subject');
          res.body.errors.subject.should.have.property('kind').eql('required');
          done();
        });
    });

    it('Must create a conversation', function (done) {
      let conversation = {
        "name": "Fulano",
        "cpf": "12345678910",
        "email": "example@example.com",
        "phone": 5598991234567,
        "state": "MARANHÃO",
        "subject": "elogio"
      }
      chai.request(server)
        .post('/conversation')
        .send(conversation)
        .end(function (error, res) {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have.property('message').eql('Informações de atendimento cadastradas com sucesso!');
          res.body.conversation.should.have.property('name');
          res.body.conversation.should.have.property('cpf');
          res.body.conversation.should.have.property('email');
          res.body.conversation.should.have.property('phone');
          res.body.conversation.should.have.property('state');
          res.body.conversation.should.have.property('subject');
          done();
        });
    });
  });

  /** 
   * Teste da rota: /PUT/:id
   */
  describe('/PUT/:id Conversation', function () {
    it('Must update a conversation by id', function (done) {
      var conversation = new Conversation({
        "name": "Beltrano",
        "cpf": "12345678910",
        "email": "example@example.com",
        "phone": 5598991234567,
        "state": "MARANHÃO",
        "subject": "reclamação"
      })
      conversation.save(function (error, conversation) {
        chai.request(server)
          .put('/conversation/' + conversation.id)
          .send({
            "attended": true
          })
          .end(function (error, res) {
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('message').eql('Informações de atendimento atualizadas com sucesso!');
            res.body.conversation.should.have.property('attended').eql(true);
            done();
          });
      });
    });
  });

  /** 
   * Teste da rota: /DELETE/:id
   */
  describe('/DELETE/:id conversation', function () {
    it('You must delete a conversation by the id', function (done) {
      var conversation = new Conversation({
        "name": "Pier",
        "cpf": "12345678910",
        "email": "example@example.com",
        "phone": 5598991234567,
        "state": "MARANHÃO",
        "subject": "reclamação"
      })
      conversation.save(function (error, conversation) {
        chai.request(server)
          .delete('/conversation/' + conversation.id)
          .end(function (error, res) {
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('message').eql('Informações de atendimento excluídas com sucesso!');
            done();
          });
      });
    });
  });
});