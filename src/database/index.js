const mongoose = require("mongoose");

mongoose.connect('mongodb://' + process.env.HOSTMONGO + process.env.MONGOBD + '?authSource=admin', {
  user: process.env.MONGO_INITDB_ROOT_USERNAME,
  pass: process.env.MONGO_INITDB_ROOT_PASSWORD,
  useUnifiedTopology: true,
  useNewUrlParser: true,
  useCreateIndex: true,
  useFindAndModify: false,
  autoIndex: true,
  poolSize: 50,
  bufferMaxEntries: 0,
  keepAlive: true,
}).then(() => {
  console.log("Conexão com o banco de dados realizada com sucesso!");
}).catch((erro) => {
  console.log("Erro: Ocorreu um erro com a conexão do banco de dados");
});

mongoose.Promise = global.Promise;

module.exports = mongoose;