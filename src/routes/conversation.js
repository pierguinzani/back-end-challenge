const express = require("express");
const router = express.Router();
const { Conversation } = require("../models/conversation");

router.get("/", (req, res) => {
    Conversation.find({}).then((conversation) => {
        return res.json(conversation);
    }).catch((error) => {
        return res.status(400).json({
            message: "Nenhuma informação de atendimento foi encontrada!"
        })
    })
});

router.get("/pending", (req, res) => {
    Conversation.find({ attended: "false" }).then((conversation) => {
        return res.json(conversation);
    }).catch((error) => {
        return res.status(400).json({
            message: "Nenhuma informação de atendimento foi encontrada!"
        })
    })
});

router.get("/:id", (req, res) => {
    Conversation.findById(req.params.id).then((conversation) => {
        return res.json(conversation);
    }).catch((error) => {
        return res.status(400).json({
            message: "Nenhuma informação de atendimento foi encontrada!",
            error
        })
    })
});

router.get('/findconversation/:name', (req, res) => {
    var regex = new RegExp(req.params.name, "i")
        , query = { name: regex };

    Conversation.find(query, function (err, conversation) {
        if (err) {
            res.json(err);
        }

        res.json(conversation);
    });

});

router.get('/lastconversation/:cpf', (req, res) => {
    Conversation.findOne({ cpf: req.params.cpf }).sort({ createdAt: -1 }).limit(1).exec(function (err, conversation) {
        if (err) {
            res.json(err);
        }

        res.json(conversation);
    });
});

router.post("/", (req, res) => {
    var conversation = new Conversation(req.body);
    conversation.save(function (error, conversation) {
        if (error) {
            res.send(error);
        } else {
            res.status(200).json({ message: "Informações de atendimento cadastradas com sucesso!", conversation });
        }

    });
});

router.put("/:id", (req, res) => {
    Conversation.findById({ _id: req.params.id }, function (error, conversation) {
        if (error)
            res.send({
                message: "Error: Ocorreu um erro na edição das informações de atendimento!",
                error
            });
        Object.assign(conversation, req.body).save(function (error, conversation) {
            if (error)
                res.send(error);
            res.json({ message: "Informações de atendimento atualizadas com sucesso!", conversation });
        });
    });
});

router.delete("/:id", (req, res) => {
    Conversation.deleteOne({ _id: req.params.id }, function (error, conversation) {
        res.json({ message: "Informações de atendimento excluídas com sucesso!", conversation });
    });
});


module.exports = router;