const mongoose = require("../database/index");
const Schema = mongoose.Schema;

const ConversationSchema = new Schema({
  name: {
    type: String,
    uppercase: true,
    trim: true,
    required: true,
  },
  cpf: {
    type: Number,
    trim: true,
    required: true,
  },
  email: {
    type: String,
    uppercase: true,
    trim: true,
    required: true,
  },
  phone: {
    type: Number,
    trim: true,
    required: true,
  },
  state: {
    type: String,
    uppercase: true,
    trim: true,
    required: true,
  },
  subject: {
    type: String,
    uppercase: true,
    trim: true,
    required: true,
  },
  attended: {
    type: Boolean,
    required: true,
    default: false,
  }
},
  {
    timestamps: true,
  });

const Conversation = mongoose.model("conversations", ConversationSchema);

module.exports = { Conversation };