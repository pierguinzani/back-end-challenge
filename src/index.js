const envPath = {
  dev: ".env.dev",
  staging: ".env.staging",
  production: ".env",
};

require("dotenv").config({
  path: envPath[process.env.NODE_ENV],
});

const express = require("express");
const bodyParser = require('body-parser');
const routeConversation = require("./routes/conversation")
const app = express();
const cors = require('cors');
const morgan = require('morgan')
const port = process.env.PORT || process.env.NODE_INTERNAL_PORT || 3001;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false}));
app.use(morgan('dev'));
app.use(cors());

app.use("/conversation", routeConversation);

app.use((req, res, next) => {
  const erro = new Error("Rota não encontrada");
  erro.status = 404;
  next(erro);
})

app.use((error, req, res, next) => {
  res.status(error.status || 500);
  return res.send({
    erro: {
      mensagem: error.message
    }
  })
})

app.listen(port, () => {
  console.log(`\nO servidor está rodando em http://localhost:${port}`);
});

module.exports = app;