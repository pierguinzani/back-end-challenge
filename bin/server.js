const App = require("../src");

const server = App();

try {
  server.start();
} catch (error) {
  console.log("[Server] Uncaught error!");
  console.log(error);
}
