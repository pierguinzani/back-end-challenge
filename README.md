# Back-end Challenge

Este projeto consiste em uma API REST para o gerenciamento do Sistema de Atendimento ao Cliente (SAC) do Mateus.

## :rocket: Recursos utilizados no desenvolvimento:

- Node.Js;
- Express.Js ~ v4.17.1;
- MongoDB ~ v3.6.3;
- Mongoose ~ v5.9.25;
- PostMan (para testar a API manualmente);
- JSON data (para retornar os dados);
- Docker ~ 19.03.8 e Docker-compose ~ v3  (para a criação dos containers)
- Mocha e Chai (para os testes automatizados);


## 🤔 Como rodar o projeto?

- Clone este repositório com
```shell
git clone https://gitlab.com/pierguinzani/back-end-challenge
```
- entre na pasta do projeto 
```shell
cd back-end-challenge
```
*p.s.: Se não tiver o git instalado em sua máquina, basta baixar o zip do projeto [Aqui](https://gitlab.com/pierguinzani/back-end-challenge/-/archive/master/back-end-challenge-master.zip), descompactar a pasta e prosseguir.*

**Você pode rodar este projeto de duas formas:**
## 1) 🐳 Com Docker 

### :construction: **Pré-requisitos**

Nessa opção você precisa já ter instalado em sua máquina:

* **Docker**: Caso não tenha, basta realizar o download da última versão [Aqui](https://docs.docker.com/engine/install/) e instalar.
* **Docker-Compose**: Caso também não tenha, basta realizar o download da última versão [Aqui](https://docs.docker.com/compose/install/) e instalar normalmente .

*p.s.: Se tiver alguma dificuldade com o docker e o docker-compose, basta seguir esse tutorial de instalação [Aqui](https://medium.com/@willianpaulo99/docker-e-docker-compose-bf967ffa84b3)!*

### :twisted_rightwards_arrows: Verificando branch
Antes de tudo, certifique-se de estar na branch **master**. Para isso, execute o comando: 

```shell
git status
```
Deve retornar o seguinte resultado no seu terminal:
```shell
No ramo master
Your branch is up to date with 'origin/master'.
```
Caso não esteja na branch **master**, basta executar o seguinte comando:
```shell
git checkout master
```

### :cyclone: Rodando a aplicação

Após a instalação do Docker e do Docker-Compose:

- execute o seguinte comando para subir o container do projeto no Docker:
```shell
docker-compose up -d --build     
``` 
Ao digitar a instrução acima, automaticamente ele irá baixar e instalar todos os programas e dependências automaticamente.


*p.s.: use  ```   sudo docker-compose up -d --build  ```no linux para rodar com privilégios.*

- A API estará disponível no endereço: `http://localhost:3001`

### :rotating_light: Testando a aplicação com Mocha e Chai
Para executar os testes automatizados da aplicação, execute o seguinte comando:

```shell
docker-compose -f docker-compose.test.yml up

``` 
*p.s.: use  ```   sudo docker-compose up -d --build  ```no linux para rodar com privilégios.*


## 2) 💻 Manualmente na sua máquina local

### :construction: **Pre-Requisitos** 

Antes de instalar as dependências no projeto, você precisa já ter instalado na sua máquina:


* **Node.Js**: Caso não tenha, basta realizar o download da última versão [Aqui](https://nodejs.org/en/)
* **MongoDb**: Caso também não tenha, basta realizar o download da última versão [Aqui](https://www.mongodb.com/download-center#community)

### :twisted_rightwards_arrows: Verificando branch

Antes de tudo, certifique-se de estar na branch **local_environment**. Para isso, execute o comando:

```shell
git status
```
Deve retornar o seguinte resultado no seu terminal:
```shell
No ramo local_environment
Your branch is up to date with 'origin/local_environment'.
```
Caso não esteja na branch **local_environment**, basta executar o seguinte comando:
```shell
git checkout local_environment
```

###  :wrench: **Instalando as Dependências**

Depois, quando estiver na pasta do projeto, basta digitar no terminal a seguinte instrução:

```
npm install
```

Ao digitar a instrução acima, automaticamente ele irá baixar todas as dependências listadas no arquivo package.json:

* `node_modules` - que contêm os packages do npm que precisará para o projeto.


### :cyclone: Rodando a aplicação

Bom, agora na mesma tela do terminal, basta iniciar o projeto localmente:

**para rodar em ambiente de produção**
```
npm run start:production
```

**para rodar em ambiente de homologação**
```
npm run start:staging
```

**para rodar em ambiente de desenvolvimento**
```
npm run start:dev
```

Depois, você precisará abrir um outro terminal na sua máquina e iniciar o MongoDB. Basta digitar na tela do novo terminal o seguinte comando:

```
mongod
```

Caso o MongoDb esteja devidamente instalado em sua máquina, ele iniciará o serviço mostrando que a port 27017 foi iniciada.


Agora, abra a página da aplicação em `http://localhost:3001/`. E pronto, a aplicação será executada de maneira local na sua máquina.       

### :rotating_light: Testando a aplicação com Mocha e Chai

Para executar os testes automatizados da aplicação rodando localmente, execute o seguinte comando:

```shell
npm run test

``` 

p.s.: no projeto, disponibilizei 2 maneiras de rodar o projeto. Fiquem à vontade para usar ou até mesmo testar ambas as conexões!! :)  
